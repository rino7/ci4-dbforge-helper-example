#!/bin/bash
phpunit --verbose --bootstrap ../vendor/autoload.php --colors=always --coverage-html ./codecoverage $1
