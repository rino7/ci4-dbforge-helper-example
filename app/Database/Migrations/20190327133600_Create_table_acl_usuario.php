<?php namespace App\Database\Migrations;

use MyDBForge;

class Migration_Create_table_acl_usuario extends \CodeIgniter\Database\Migration
{

    public function up()
    {
        $fields = [
            'id_acl_usuario' => MyDBForge\DBForgeHelper::pkField(),
            'nombre' => MyDBForge\DBForgeHelper::varcharField(),
            'apellido' => MyDBForge\DBForgeHelper::varcharField(),
            'email' => MyDBForge\DBForgeHelper::varcharField(),
            'estado' => MyDBForge\DBForgeHelper::enum(),
            'eliminado' => MyDBForge\DBForgeHelper::binaryField(false, 0),

        ];
        $this->forge->addPrimaryKey('id_acl_usuario');
        $this->forge->addField($fields);
        $this->forge->createTable('acl_usuario', true);
    }

    public function down()
    {
        $this->forge->dropTable('acl_usuario', true);
    }
}
